// 과목추가의 대학교에 따른 리턴하는 값들의 데이터
export const purpose = [
  "초등수학",
  "중학수학",
  "고등수학",
  "대학수학",
  "취미 및 기타",
];
export const elementary = ["모두", 1, 2, 3, 4, 5, 6];
export const middle = ["모두", 1, 2, 3];
export const high = ["모두", 1, 2, 3];
export const university = ["모두", 1, 2, 3, 4];
export const prepare = ["모두", "교과과정/내신", "수능", "올림피아드", "기타"];
export const level = [
  "모두",
  "기초",
  "초급",
  "중위권",
  "상위권10%",
  "1등급",
  "최상위1%",
];
export const gender = ["성별무관", "남자", "여자"];
export const online = ["모두", "가능", "제외"];
export const hobby = ["모두"];
export const question = ["기능 오류", "기능 제안", "기타 사항"];
export const INSTITUTIONAL_UNIVERSITY_RANKINGS = [
  "QS",
  "THE",
  "ARWU",
  "중앙일보",
];
export const MAP_TEXT = {
  A: "모두",
  Y: "가능",
  N: "제외",
};
