export const signInText = {
  APP: "맞츔ID",
  GG: "GOOGLE",
  KAKAO: "KAKAO",
  NAVER: "NAVER",
};
export const MAX_LENGTH_IN_MILLISECOND = 60 * 1000;
export const MAX_SIZE_IN_BYTE = 500 * 1024 * 1024;
export const VIDEO_RECORD_TYPES = {
  STYLE: "스타일",
  SKILL: "스킬",
  DATA: "교재",
  COMMUNICATION: "소통",
};

export const VIDEO_RECORD_TYPE_TITLE = {
  STYLE: "스타일",
  SKILL: "수업 방식 및 티칭 스킬",
  DATA: "교재 및 자료",
  COMMUNICATION: "학생과의 소통",
};

export const PRICE_NOT_SET = "미설정";
