export const PRICE = {
  TITLE: "가격",
  ASCENDING: "낮은 가격 순",
  DESCENDING: "높은 가격 순",
};

export const AGE = {
  TITLE: "나이",
  ASCENDING: "낮은 나이 순",
  DESCENDING: "높은 나이 순",
};
export const SORT_MAP = {
  [PRICE.ASCENDING]: "price1",
  [PRICE.DESCENDING]: "price0",
  [AGE.ASCENDING]: "age1",
  [AGE.DESCENDING]: "age0",
  QS: "QS",
  THE: "THE",
  ARWU: "ARWU",
  중앙일보: "middle",
};

export const NOTIFICATION_TYPE = {
  SEND_MATCHING: 0,
  ACCEPT_MATCHING: 1,
  CHAT: 2,
};

export const NOTIFICATION_TEXT = {
  [NOTIFICATION_TYPE.SEND_MATCHING]: "user_name 님이 매칭을 신청하였습니다",
  [NOTIFICATION_TYPE.ACCEPT_MATCHING]: "user_name 님이 매칭을 수락하였습니다",
  [NOTIFICATION_TYPE.CHAT]: "user_name: message",
};

export const EXPO_PUSH_TOKENS = "ExpoPushTokens";
export const USER_ACTIVITY = "UserActivities";
export const GUIDE_LINK = "https://www.matchuum.ai/profile.html";
export const SERVER_ERROR = "서버에 문제가 있습니다.";
export const REPORTS = [
  "비매너 행위",
  "스팸성 홍보",
  "불법 행위",
  "사기 피해",
  "정보 부정확",
  "기타사유",
];

export const STEP = {
  PHONE: 0,
  PASSWORD: 1,
  USERNAME: 2,
  SUCCESS: 3,
}
