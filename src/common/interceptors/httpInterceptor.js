import * as SecureStore from "expo-secure-store";
import { PUBLIC_API } from "../../config/configureApi";
const AUTHORIZATION_HEADER = "Authorization";
let url = "";
export function setupAxiosInterceptors(axiosInstance) {
  axiosInstance.interceptors.request.use(
    async (config) => {
      url = config?.url;
      const token = await SecureStore.getItemAsync("token");
      const isNonPublicAPI = PUBLIC_API.every((item) => !url.includes(item));
      if (isNonPublicAPI && !!token) {
        config.headers[AUTHORIZATION_HEADER] = `Bearer ${token}`;
      }
      return config;
    },
    (error) => Promise.reject(error)
  );
  axiosInstance.interceptors.response.use(
    (response) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
    },
    (error) => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
    }
  );
  return axiosInstance;
}
