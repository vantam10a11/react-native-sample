const regexPassword = /^[a-zA-Z0-9 ]{6,}$/g;
export const regexEscapeCharacter = /[\{\}\[\]\/?.,;:|\)*~`!^\-+<>@\#$%&\\\=\(\'\"]/gi;
export const testPassword = (password) => {
  return regexPassword.test(password);
};
export const testNick = (nick) => {
  return regexEscapeCharacter.test(nick);
};
export const regexNumberPrice = /\B(?=(\d{3})+(?!\d))/g;
// Because the dot is a special character in regex you need to escape it to match it literally: \.
export const regexFileExtension = /\.[0-9a-z]+$/i;
