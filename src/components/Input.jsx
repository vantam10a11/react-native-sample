import React, { forwardRef, useState } from "react";
import styled from "styled-components/native";
import { submitColor } from "../styles/colors";

const StyledTextInput = styled.TextInput`
  border: 1px solid ${({ color }) => color || "black"};
  border-radius: ${({ borderRadius }) => borderRadius || 4}px;
  padding-left: 14px;
  font-size: ${({ fontSize }) => fontSize || 14}px;
  height: 48px;
  font-family: "NotoSansRegular";
  color: #333333;
  justify-content: center;

  width: ${({ width }) => width || 100}%;
`;

const Input = ({
  maxLength,
  color,
  borderRadius,
  placeholder,
  keyboardType,
  width,
  onChange,
  fontSize,
  value,
  autoComplete = "off",
  autoFocus,
  isTransparent = false,
  secureTextEntry,
  editable = true,
  defaultValue = "",
}) => {
  // let input = null;
  const [text, setText] = useState("");
  const [isFocused, setIsFocused] = useState(false);
  const handleFocus = () => {
    setIsFocused(true);
  };
  const handleBlur = () => {
    setIsFocused(false);
  };

  const onChangeText = (text) => {
    onChange(text);
    setText(text);
  };

  return (
    <StyledTextInput
      onFocus={handleFocus}
      onBlur={handleBlur}
      maxLength={maxLength}
      fontSize={fontSize}
      onChangeText={onChangeText}
      keyboardType={keyboardType}
      width={width}
      color={isTransparent ? "transparent" : isFocused ? submitColor : color}
      borderRadius={borderRadius}
      placeholder={placeholder}
      value={value ?? text}
      autoComplete={autoComplete}
      autoFocus={autoFocus}
      importantForAutofill="yes"
      selectionColor={submitColor}
      secureTextEntry={secureTextEntry}
      editable={editable}
      defaultValue={defaultValue}
    />
  );
};

export default Input;
