import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  Alert,
  Dimensions,
  Image,
  KeyboardAvoidingView,
  Text,
} from "react-native";
import styled from "styled-components/native";
import { useFonts } from "expo-font";
import {
  doneSubmitColor,
  fontAccentGreyColor,
  fontGreyColor,
  subGreyColor,
  submitColor,
} from "../../styles/colors";
import Input from "../../components/Input";
const PhoneContainer = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 30px;
  margin-bottom: 15px;
  align-items: center;
  border: 1px solid ${({ borderColor }) => borderColor};
  border-radius: 4px;
  padding-left: 16px;
  width: 100%;
`;

const PhoneCode = styled.Text`
  font-size: 14px;
  font-weight: 600;
  font-family: "NotoSansMedium";
  color: ${({ color }) => color};
`;

const PhoneTextInput = styled.TextInput`
  padding-left: 14px;
  font-size: 14px;
  height: 48px;
  font-family: "NotoSansRegular";
  color: ${({ textColor }) => textColor};
  flex: 1;
`;

const CustomPhoneInput = ({ onChangePhone, isDisabled = false }) => {
  const [isFocused, setIsFocused] = useState(false);
  const [text, onChangeText] = useState("");
  const handleFocus = () => {
    setIsFocused(true);
  };
  const handleBlur = () => {
    setIsFocused(false);
  };
  return (
    <PhoneContainer borderColor={isFocused ? submitColor : "#e1e1e1"}>
      <PhoneCode color={isDisabled ? fontGreyColor : "#2273b1"}>
        KR +82
      </PhoneCode>
      <Text
        style={{
          marginLeft: 20,
          marginRight: 10,
          marginBottom: 2,
          color: fontGreyColor,
        }}
      >
        |
      </Text>
      <PhoneTextInput
        value={text}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onChangeText={(text) => {
          onChangePhone(text);
          onChangeText(text);
        }}
        keyboardType="numeric"
        placeholder="- 제외한 숫자만 입력"
        maxLength={11}
        textColor={isDisabled ? fontGreyColor : "#333333"}
      />
    </PhoneContainer>
  );
};

export default CustomPhoneInput;
