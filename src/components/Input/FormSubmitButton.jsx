import React from "react";
import styled from "styled-components/native";
import { useForm } from "react-hook-form";
import { disableColor, submitColor } from "../../styles/colors";

const StyledButton = styled.TouchableOpacity`
  border-radius: 4px;
  background-color: ${({ backgroundColor }) => backgroundColor || "white"};
  justify-content: center;
  height: 48px;
  margin-top: 10%;
`;

const StyledText = styled.Text`
  color: white;
  font-family: "NotoSansMedium";
  font-size: 14px;
  text-align: center;
  line-height: 16px;
`;

const FormSubmitButton = ({
  title,
  backgroundColor,
  fontColor,
  handleSubmit,
  disabled,
}) => {
  return (
    <StyledButton
      onPress={handleSubmit}
      backgroundColor={disabled ? disableColor : submitColor}
      disabled={disabled}
    >
      <StyledText fontColor={fontColor}>{title}</StyledText>
    </StyledButton>
  );
};

export default FormSubmitButton;
