import React, { useState } from "react";
import styled from "styled-components/native";
import { Controller, useForm } from "react-hook-form";
import {
  backgroundColor,
  disableColor,
  submitColor,
} from "../../styles/colors";

const Label = styled.Text`
  font-size: 15px;
  font-family: "NotoSansMedium";
  font-weight: bold;
  color: #1c1c1e;
  margin: 10px 0;
`;

const BoxContainer = styled.View`
  height: 48px;
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 10px 0;
`;

const RoundCheckbox = styled.View`
  height: 7px;
  width: 7px;
  border-radius: 3.5px;
  background-color: ${submitColor};
  margin-left: 8px;
`;

const StyledText = styled.Text`
  font-size: 15px;
  font-family: "NotoSansMedium";
`;

const StyledTextInput = styled.TextInput`
  border: 2px solid ${({ color }) => color || "black"};
  background-color: ${({ background }) => background};
  border-radius: 4px;
  padding-left: 15px;
  font-size: ${({ fontSize }) => fontSize || 15}px;
  min-height: 48px;
  font-family: "NotoSansMedium";
  color: #1c1c1e;
  margin-bottom: 20px;
`;

const FormTextInput = ({
  maxLength,
  color,
  placeholder,
  keyboardType,
  onChange,
  value,
  label,
  defaultValue,
  secureTextEntry,
  keyName,
  control,
  multiline,
  numberOfLines,
  isRequired,
  fontSize,
}) => {
  const [text, setText] = useState("");
  const [isFocused, setIsFocused] = useState(false);
  const [isSelected, setIsSelected] = useState(false);
  const handleFocus = () => {
    setIsFocused(true);
  };
  const handleBlur = () => {
    setIsFocused(false);
  };
  return (
    <>
      {label && (
        <BoxContainer>
          <StyledText>{label}</StyledText>
          {isRequired && <RoundCheckbox />}
        </BoxContainer>
      )}
      <Controller
        control={control}
        render={({ field: { onChange, value, ref } }) => {
          return (
            <StyledTextInput
              fontSize={fontSize}
              maxLength={maxLength}
              onFocus={handleFocus}
              onBlur={handleBlur}
              onChangeText={(text) => {
                onChange(text);
                setText(text);
              }}
              ref={ref}
              keyboardType={keyboardType}
              color={
                isFocused ? submitColor : isSelected ? disableColor : color
              }
              background={
                multiline
                  ? isSelected || isFocused
                    ? "white"
                    : backgroundColor
                  : "white"
              }
              placeholder={placeholder}
              value={value}
              secureTextEntry={secureTextEntry}
              multiline={multiline}
              numberOfLines={numberOfLines}
              textAlignVertical={multiline ? "top" : undefined}
              onEndEditing={({ nativeEvent: { text } }) => {
                if (text != "") setIsSelected(true);
              }}
            />
          );
        }}
        name={keyName}
        // defaultValue={defaultValue}
      />
    </>
  );
};

export default FormTextInput;
