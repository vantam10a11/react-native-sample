import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
const TopTab = ({ tabs }) => {
  const Tab = createMaterialTopTabNavigator();
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarIndicatorStyle: {
          backgroundColor: "#555555",
        },
      }}
    >
      {tabs?.map(({ name, children, onPressTab }) => (
        <Tab.Screen
          key={name}
          name={name}
          children={() => children}
          listeners={
            onPressTab && {
              tabPress: (e) => {
                onPressTab();
              },
            }
          }
        />
      ))}
    </Tab.Navigator>
  );
};
export default TopTab;
