import React from "react";
import { View, Modal, Text, TouchableWithoutFeedback } from "react-native";
import styled from "styled-components/native";
import {
  disableColor,
  fontGreyColor,
  submitColor,
  subWhiteColor,
} from "../styles/colors";
import CustomButton from "./CustomButton";
const Title = styled.Text`
  font-size: 16px;
  text-align: center;
  margin-bottom: 10px;
  font-family: "NotoSansMedium";
`;
const ModalView = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.5);
  justify-content: center;
  align-content: center;
  align-items: center;
  border-radius: 4px;
`;
const Div = styled.View`
  background-color: #fff;
  padding-top: 20px;
  padding-left: 5%;
  padding-right: 5%;
  width: 90%;
`;
const ButtonContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10%;
  width: 100%;
`;

// 회원쪽의 재사용 모달 컴포넌트 (로그아웃, 회원탈퇴)
const ModalComponent = ({
  modal,
  closeModal,
  title,
  description,
  outButtonTitle,
  backButtonTitle,
  removeUser,
  isLoading = false,
}) => {
  const onPressMainButton = () => {
    removeUser && removeUser();
    closeModal();
  }
  return (
    <Modal
      animationType="fade"
      transparent
      visible={modal}
      onRequestClose={closeModal}
    >
      <TouchableWithoutFeedback onPress={closeModal}>
        <ModalView>
          <Div>
            {title && <Title>{title}</Title>}
            {description && (
              <Text
                style={{
                  marginBottom: "10%",
                  textAlign: "center",
                  lineHeight: 18,
                  fontSize: 13,
                }}
              >
                {description}
              </Text>
            )}
            <ButtonContainer>
              {backButtonTitle && (
                <CustomButton
                  title={backButtonTitle ?? "확인"}
                  onPress={closeModal}
                  backgroundColor={fontGreyColor}
                  width={45}
                  fontColor={subWhiteColor}
                  borderColor="transparent"
                />
              )}
              <CustomButton
                title={outButtonTitle ?? "확인"}
                onPress={onPressMainButton}
                backgroundColor={isLoading ? fontGreyColor : submitColor}
                width={!!backButtonTitle ? 45 : undefined}
                fontColor={subWhiteColor}
                disabled={isLoading}
              />
            </ButtonContainer>
          </Div>
        </ModalView>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default ModalComponent;
