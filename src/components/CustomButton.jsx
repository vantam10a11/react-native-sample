import { useFonts } from "expo-font";
import React from "react";
import styled from "styled-components/native";
import { fontGreyColor, mainColor } from "../styles/colors";

const StyledButton = styled.TouchableOpacity`
  border-radius: ${({ borderRadius }) => borderRadius || 4}px;
  background-color: ${({ backgroundColor }) => backgroundColor || mainColor};
  width: ${({ width }) => width || 100}%;
  justify-content: center;
  height: ${({ height }) => height || "45px"};
  margin-left: ${({ marginLeft }) => marginLeft || 0}px;
  border: 1px solid ${({ borderColor }) => borderColor || "transparent"};
`;

const StyledText = styled.Text`
  color: ${({ fontColor }) => fontColor || "white"};
  font-family: "NotoSansMedium";
  font-size: 14px;
  text-align: center;
  line-height: 16px;
`;

// 취향 저격 스타일 , 수업방식 등등의 버튼 컴포넌트들
const CustomButton = ({
  title,
  backgroundColor,
  fontColor,
  width,
  marginLeft,
  onPress,
  borderColor,
  height,
  style,
  activeOpacity,
  borderRadius,
  disabled,
}) => {
  const [loaded] = useFonts({
    NotoSansMedium: require("../../assets/fonts/NotoSansKR-Medium.otf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <StyledButton
      onPress={onPress}
      backgroundColor={backgroundColor}
      width={width}
      marginLeft={marginLeft}
      borderColor={borderColor}
      height={height}
      style={style}
      activeOpacity={activeOpacity}
      borderRadius={borderRadius}
      disabled={disabled}
    >
      <StyledText fontColor={fontColor}>{title}</StyledText>
    </StyledButton>
  );
};

export default CustomButton;
