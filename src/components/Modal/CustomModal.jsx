import { useFonts } from "expo-font";
import React from "react";
import { TouchableOpacity } from "react-native";
import { View, Modal } from "react-native";
import styled from "styled-components/native";

import { submitColor, subWhiteColor } from "../../styles/colors";
import CustomButton from "../CustomButton";
import { SvgXml } from "react-native-svg";
import { exitPage } from "../../styles/icon";

const ModalView = styled.View`
  flex: 1;
`;
const Header = styled.View`
  height: 10%;
  width: 100%;
  background-color: #ffffff;
  justify-content: space-between;
  align-items: flex-end;
  padding: 0 12px 25px;
  flex-direction: row;
`;
const HeaderText = styled.Text`
  font-size: 16px;
  font-family: "NotoSansMedium";
  line-height: 18px;
`;

const Content = styled.View`
  width: 88%;
  height: 80%;
  align-self: center;
  position: relative;
`;

const Bottom = styled.View`
  position: absolute;
  bottom: 30px;
  width: 88%;
  align-self: center;
`;

const CustomModal = ({
  modalVisible,
  closeModal,
  title,
  children,
  isVisibleBottomButton,
  onPressBottomButton,
  bottomButtonTitle,
}) => {
  const [loaded] = useFonts({
    NotoSansMedium: require("../../../assets/fonts/NotoSansKR-Medium.otf"),
    NotoSansRegular: require("../../../assets/fonts/NotoSansKR-Regular.otf"),
  });

  if (!loaded) {
    return null;
  }
  return (
    <Modal
      animationType="fade"
      transparent={false}
      visible={modalVisible}
      onRequestClose={closeModal}
    >
      <ModalView>
        <Header>
          <View
            style={{
              width: 16,
              height: 16,
              backgroundColor: "rgba(255,255,255,0.0)",
            }}
          />
          <HeaderText>{title}</HeaderText>
          <TouchableOpacity
            onPress={closeModal}
            activeOpacity={0.8}
            hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
          >
            <SvgXml xml={exitPage} />
          </TouchableOpacity>
        </Header>
        <Content>{children}</Content>
        {isVisibleBottomButton && (
          <Bottom>
            <CustomButton
              onPress={() => {
                onPressBottomButton();
                closeModal();
              }}
              title={bottomButtonTitle}
              backgroundColor={submitColor}
              fontColor={subWhiteColor}
            />
          </Bottom>
        )}
      </ModalView>
    </Modal>
  );
};

export default CustomModal;
