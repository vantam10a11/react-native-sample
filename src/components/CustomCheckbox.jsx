import { useFonts } from "expo-font";
import React, { useEffect, useState } from "react";
import styled from "styled-components/native";
import { subGreyColor, submitColor, subWhiteColor } from "../styles/colors";

const BoxContainer = styled.View`
  border-radius: 4px;
  height: 48px;
  border: ${({ isBlock }) => (isBlock ? "0px" : "1px")} solid #e1e1e1;
  padding: 10px ${({ isBlock }) => (isBlock ? "0px" : "8px")};
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 10px 0px;
`;

const RoundCheckbox = styled.TouchableOpacity`
  height: 15px;
  width: 15px;
  border-radius: 7.5px;
  border: 1px solid ${({ borderColor }) => borderColor || "transparent"};
  background-color: ${({ backgroundColor }) => backgroundColor};
`;

const Terms = styled.TouchableOpacity`
  margin-left: ${({ isBlock }) => (isBlock ? "0px" : "20px")};
`;

const StyledText = styled.Text`
  margin-left: 10px;
  font-family: "NotoSansMedium";
  font-weight: bold;
  font-size: 14px;
  text-align: center;
  line-height: 19px;
`;

const CustomCheckbox = ({
  title,
  backgroundColor,
  fontColor,
  onPress,
  borderColor,
  value,
  onPressTitle,
  onValueChange,
  isBlock = false,
}) => {
  const [isChecked, setIsChecked] = useState(false);
  const [loaded] = useFonts({
    NotoSansMedium: require("../../assets/fonts/NotoSansKR-Medium.otf"),
  });
  useEffect(() => {
    onValueChange(isChecked);
  }, [isChecked]);
  useEffect(() => {
    if (value != null) {
      setIsChecked(value);
    }
  }, [value]);

  if (!loaded) {
    return null;
  }

  return (
    <BoxContainer isBlock={isBlock}>
      <RoundCheckbox
        hitSlop={{ top: 15, bottom: 15, left: 15, right: 15 }}
        onPress={() => setIsChecked(!isChecked)}
        backgroundColor={isChecked ? submitColor : subWhiteColor}
        borderColor={isChecked ? undefined : subGreyColor}
      ></RoundCheckbox>
      <Terms onPress={onPressTitle} isBlock={isBlock}>
        <StyledText>{title}</StyledText>
      </Terms>
    </BoxContainer>
  );
};

export default CustomCheckbox;
