import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { useFonts } from "expo-font";
import SignUp from "./screens/SignUp/SignUp";
import { subWhiteColor } from "./styles/colors";
import React from "react";
import { isAndroidPlatform } from "./common/platform";
import { StatusBar } from "expo-status-bar";
const Stack = createStackNavigator();

export default function App() {
  const [loaded] = useFonts({
    NotoSansMedium: require("../assets/fonts/NotoSansKR-Medium.otf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <>
      {isAndroidPlatform && <StatusBar backgroundColor="transparent" />}
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName={"SignUp"}
          screenOptions={{
            cardStyle: {
              backgroundColor: subWhiteColor,
            },
            headerTintColor: "black",
            headerBackTitleVisible: false,
            headerTitleAlign: "center",
            headerTitleStyle: {
              fontFamily: "NotoSansMedium",
            },
            headerStyle: {
              elevation: 4,
              shadowColor: "black",
            },
          }}
        >
          <Stack.Screen
            component={SignUp}
            name="SignUp"
            options={{
              headerTitle: "회원가입",
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}
