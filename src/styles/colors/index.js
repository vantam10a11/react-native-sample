// Pale: 옅은 , Indigo: 남색 , Accent: 짙은
// Colors
export const mainColor = '#FF206F';
export const subWhiteColor = '#FFFFFF';
export const subGreyColor = '#DBDBDB';
export const subBlueColor = '#1A73E9';
export const subOrangeColor = '#FF5050';

// font Colors
export const blackColor = '#333333';
export const fontAccentGreyColor = '#999999';
export const fontGreyColor = '#AAAAAA';
export const fontOrangeColor = '#FE4D43';

// Button Color
export const submitColor = '#FF216E';
export const disableColor = '#FFBCD3';
export const doneSubmitColor = '#AAAAAA';
// Background Color
export const backgroundColor = '#E1E1E1';
