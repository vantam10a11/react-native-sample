import axios from 'axios';
const instance = axios.create();
// 이용약관
export const getServiceInformation = async () => {
  const response = await instance.get('https://matchuum.com/infomation/list');
  const data = await response.data;
  return data;
};
