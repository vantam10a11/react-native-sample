import React from "react";
import styled from "styled-components/native";

const Container = styled.ScrollView`
  align-self: center;
  margin-top: 32px;
`;
const StyledText = styled.Text`
  font-family: "NotoSansRegular";
  font-size: 14px;
  line-height: 22px;
`;

const PrivacyPolicy = ({ content, width }) => {
  return (
    <Container
      style={{
        width: width ?? "100%",
      }}
    >
      <StyledText>{content}</StyledText>
    </Container>
  );
};

export default PrivacyPolicy;
