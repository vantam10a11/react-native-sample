import { useFonts } from "expo-font";
import React, { useContext, useEffect, useState } from "react";
import { Dimensions, ScrollView } from "react-native";
import styled from "styled-components/native";
import Input from "../../components/Input";
import {
  disableColor,
  fontGreyColor,
  subGreyColor,
  submitColor,
} from "../../styles/colors";
import CustomButton from "../../components/CustomButton";
import CustomCheckbox from "../../components/CustomCheckbox";
import PrivacyPolicy from "../ServiceConfirm/PrivacyPolicy";
import { getServiceInformation } from "../../service/serviceInformation";
import CustomModal from "../../components/Modal/CustomModal";
import { testNick } from "../../utils/regex";
import { StateContext } from "./StateProvider";
import { STEP } from "../../common/constants";

const Container = styled.View`
  margin-bottom: 50px;
`;
const StyledView = styled.View`
  width: ${({ width }) => width - 40}px;
  height: ${({ height }) => height}px;
  align-self: center;
  margin-top: 9%;
  height: 100%;
  flex: 1;
`;
const StyledText = styled.Text`
  font-size: ${({ textFont }) => textFont || 14}px;
  font-family: "NotoSansRegular";
  color: ${({ color }) => (color ? color : "black")};
  height: ${({ height }) => height || 5}%;
  font-weight: bold;
`;

const UserNick = () => {
  const { setStep } = useContext(StateContext);
  const [numberChecked, setNumberChecked] = useState(1);
  const [currentTermIndex, setCurrentTermIndex] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [nickName, setNickName] = useState("");
  const [policy, setPolicy] = useState([]);
  const [isAccept, setIsAccept] = useState(false);

  useEffect(() => {
    Promise.all([getServiceInformation()]).then(([policy]) => {
      setPolicy(policy?.map(({ content }) => content));
    });
  }, []);

  const terms = [
    { title: "이용약관", content: policy[0] },
    { title: "개인정보처리방침 (필수)", content: policy[1] },
    { title: "위치기반서비스 이용약관 (필수)", content: policy[2] },
  ];

  const { width, height } = Dimensions.get("window");
  const [loaded] = useFonts({
    NotoSansMedium: require("../../../assets/fonts/NotoSansKR-Medium.otf"),
    NotoSansRegular: require("../../../assets/fonts/NotoSansKR-Regular.otf"),
    NotoSansLight: require("../../../assets/fonts/NotoSansKR-Light.otf"),
  });

  if (!loaded) {
    return null;
  }

  const nickNameEntered = (text) => {
    setNickName(text);
  };

  const submitNickNameEntered = () => {
    setIsLoading(true);
    setStep(STEP.SUCCESS);
  };
  const isDisabled =
    numberChecked != 3 || nickName.length < 2 || testNick(nickName);

  const modalProps = {
    modalVisible,
    closeModal: () => setModalVisible(false),
    title: terms[currentTermIndex].title,
    isVisibleBottomButton: true,
    bottomButtonTitle: "약관에 동의함",
    onPressBottomButton: () => setIsAccept(true),
  };

  const onCheck = (isChecked) => {
    setNumberChecked(isChecked ? numberChecked + 1 : numberChecked - 1);
  };
  const onPressTitle = (index) => {
    setModalVisible(true);
    setCurrentTermIndex(index);
    setIsAccept(false);
  };
  return (
    <>
      <StyledView width={width} height={height}>
        <StyledText textFont={16} height={5}>
          닉네임
        </StyledText>
        <Container>
          <Input
            onChange={nickNameEntered}
            fontSize={12}
            color={subGreyColor}
            placeholder="2자 이상 입력"
            maxLength={8}
            autoComplete="username"
          />
        </Container>
        {terms?.map(({ title }, index) => (
          <CustomCheckbox
            key={index.toString()}
            onValueChange={onCheck}
            title={title}
            onPressTitle={() => onPressTitle(index)}
            value={index == currentTermIndex && isAccept ? true : null}
          />
        ))}
        <CustomButton
          style={{ marginTop: 50 }}
          onPress={submitNickNameEntered}
          title="회원가입"
          backgroundColor={
            isLoading ? fontGreyColor : isDisabled ? disableColor : submitColor
          }
          disabled={isDisabled || isLoading}
        />
        <CustomModal {...modalProps}>
          <ScrollView>
            <PrivacyPolicy content={terms[currentTermIndex].content} />
          </ScrollView>
        </CustomModal>
      </StyledView>
    </>
  );
};

export default UserNick;
