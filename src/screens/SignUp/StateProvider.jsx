import React, { useState, createContext, useMemo } from "react";
import { STEP } from "../../common/constants";
export const StateContext = createContext();
const StateProvider = ({ children }) => {
  const [step, setStep] = useState(STEP.PHONE);
  const [credential, setCredential] = useState(null);
  const value = useMemo(() => ({ step, setStep, credential, setCredential }));
  return (
    <StateContext.Provider value={value}>{children}</StateContext.Provider>
  );
};

export default StateProvider;
