import React, { useLayoutEffect } from "react";
import styled from "styled-components/native";
import { SvgXml } from "react-native-svg";
import { Dimensions } from "react-native";
import { mainColor } from "../../styles/colors";
import { logo1 } from "../../styles/icon";

const Container = styled.View`
  display: flex;
  flex-direction: column;
  position: relative;
  justify-content: center;
  align-items: center;
`;

const BlackText = styled.Text`
  font-size: 18px;
  font-family: "NotoSansRegular";
  line-height: 25px;
  text-align: center;
  margin-top: 27px;
  font-weight: bold;
`;
const PinkText = styled.Text`
  font-size: 12px;
  font-family: "NotoSansRegular";
  line-height: 16px;
  text-align: center;
  color: ${mainColor};
  margin-top: ${({ noHeight }) => (noHeight ? 0 : 40)}px;
`;

const TextButton = styled.TouchableOpacity`
  margin-top: 3px;
`;
const SignUpSuccess = ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  });
  const { height } = Dimensions.get("window");

  return (
    <Container height={height}>
      <SvgXml xml={logo1} width={150} height={150} />
      <BlackText>{`회원가입 완료!${"\n"}수학맞츔 회원이 되신것을 환영합니다.`}</BlackText>
      <PinkText>수수료 0%! 인공지능 과의 매칭 서비스를 만나보세요!</PinkText>
      <TextButton>
        <PinkText noHeight style={{ textDecorationLine: "underline" }}>
          맞츔하기
        </PinkText>
      </TextButton>
    </Container>
  );
};
export default SignUpSuccess;
