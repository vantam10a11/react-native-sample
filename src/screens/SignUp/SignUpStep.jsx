import React, { useContext } from "react";
import UserNick from "./UserNick";
import SignUpTab from "./SignUpTab";
import SignUpSuccess from "./SignUpSuccess";
import Password from "./Password";
import { StateContext } from "./StateProvider";
import { STEP } from "../../common/constants";
const SignUpStep = ({ navigation }) => {
  const { step } = useContext(StateContext);
  return (
    <>
      {step === STEP.PHONE && <SignUpTab />}
      {step === STEP.PASSWORD && <Password />}
      {step === STEP.USERNAME && <UserNick />}
      {step === STEP.SUCCESS && <SignUpSuccess navigation={navigation} />}
    </>
  );
};

export default SignUpStep;
