import React from "react";
import ModalComponent from "../../components/ModalComponent";

const CheckPhoneMailModal = ({
  isNewPhone,
  modalVisible,
  closeModal,
  isMail,
}) => {
  const textForMail = isMail
    ? `메일로 인증번호가 전송되었습니다.${"\n"}인증번호를 입력해 주세요.${"\n"}메일이 오지 않았다면, 스팸메일함을 확인해 주시기 바랍니다.`
    : `인증번호가 전송되었습니다.${"\n"}인증번호 6자리를 입력해 주세요.`;
  return (
    <ModalComponent
      modal={modalVisible}
      closeModal={closeModal}
      description={isNewPhone ? textForMail : "이미 가입하신 회원입니다."}
    />
  );
};

export default CheckPhoneMailModal;
