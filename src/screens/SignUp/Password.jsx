import React, { useContext } from "react";
import { Dimensions } from "react-native";
import styled from "styled-components/native";
import { useFonts } from "expo-font";
import { subGreyColor } from "../../styles/colors";
import { useForm } from "react-hook-form";
import FormTextInput from "../../components/Input/FormTextInput";
import FormSubmitButton from "../../components/Input/FormSubmitButton";
import { StateContext } from "./StateProvider";
import { testPassword } from "../../utils/regex";
import { STEP } from "../../common/constants";

const Container = styled.View`
  display: flex;
  flex-direction: column;
  position: relative;
  align-self: center;
  width: 88%;
  height: ${({ height }) => height}px;
  margin-top: 10px;
`;

const Password = () => {
  const { setStep, credential, setCredential } = useContext(StateContext);
  const [loaded] = useFonts({
    NotoSansMedium: require("../../../assets/fonts/NotoSansKR-Medium.otf"),
  });
  const { handleSubmit, control, watch } = useForm();
  const onFormSubmit = ({ password }) => {
    setCredential({ ...credential, password });
    setStep(STEP.USERNAME);
  };
  const [password, rePassword] = watch(["password", "rePassword"]);
  const isDisabled =
    password == undefined ||
    password == "" ||
    !testPassword(password) ||
    password !== rePassword;

  if (!loaded) {
    return null;
  }

  return (
    <Container height={Dimensions.get("window").height}>
      <FormTextInput
        control={control}
        label="비밀번호"
        placeholder="영문, 숫자 포함 6자 이상 입력"
        color={subGreyColor}
        secureTextEntry
        keyName="password"
      />
      <FormTextInput
        control={control}
        label="비밀번호를 재 입력해 주세요"
        placeholder="영문, 숫자 포함 6자 이상 입력"
        color={subGreyColor}
        secureTextEntry
        keyName="rePassword"
      />
      <FormSubmitButton
        title="다음"
        disabled={isDisabled}
        handleSubmit={handleSubmit(onFormSubmit)}
      />
    </Container>
  );
};

export default Password;
