import React from "react";
import TopTab from "../../components/Tab/TopTab";
import SignUpWithPhone from "./SignUpWithPhone";
import SignUpWithMail from "./SignUpWithMail";
const SignUpTab = React.memo(() => {
  const tabs = [
    { name: "휴대폰", children: <SignUpWithPhone /> },
    {
      name: "이메일",
      children: <SignUpWithMail />,
    },
  ];
  return <TopTab tabs={tabs} />;
});

export default SignUpTab;
