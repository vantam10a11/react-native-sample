import React, { useCallback, useContext, useEffect, useState } from "react";
import { Alert, Dimensions } from "react-native";
import styled from "styled-components/native";
import { useFonts } from "expo-font";
import {
  disableColor,
  doneSubmitColor,
  subBlueColor,
  subGreyColor,
  submitColor,
} from "../../styles/colors";
import Input from "../../components/Input";
import CustomButton from "../../components/CustomButton";
import CheckPhoneMailModal from "./CheckPhoneMailModal";
import { StateContext } from "./StateProvider";
import CustomPhoneInput from "../../components/Input/CustomPhoneInput";
import { SERVER_ERROR, STEP } from "../../common/constants";

const Container = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 30px;
  margin-bottom: 15px;
  align-items: center;
`;

const StyledView = styled.View`
  width: 100%;
  align-self: center;
  height: 100%;
  flex: 1;
  background-color: white;
  padding-left: 20px;
  padding-right: 20px;
`;
const Timer = styled.Text`
  position: absolute;
  right: 10%;
  z-index: 99;
  color: ${subBlueColor};
`;

const SignUpWithPhone = () => {
  const { setStep, setCredential } = useContext(StateContext);
  const [phone, setPhone] = useState("");
  const [otp, setOtp] = useState("");
  const isNewPhone = true;
  const [isLoadingCheckPhone, setLoadingCheckPhone] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [timerOn, setTimerOn] = useState(false);
  const [minutes, setMinutes] = useState(1);
  const [seconds, setSeconds] = useState(0);
  const closeModal = useCallback(() => setModalVisible(false), [modalVisible]);

  useEffect(() => {
    if (timerOn) {
      const countdown = setInterval(() => {
        if (parseInt(seconds) > 0) {
          setSeconds(parseInt(seconds) - 1);
        }
        if (parseInt(seconds) === 0) {
          if (parseInt(minutes) === 0) {
            setTimerOn(false);
            clearInterval(countdown);
          } else {
            setMinutes(parseInt(minutes) - 1);
            setSeconds(59);
          }
        }
      }, 1000);

      return () => clearInterval(countdown);
    }
  }, [minutes, seconds, timerOn]);

  const { width } = Dimensions.get("window");
  const [loaded] = useFonts({
    NotoSansMedium: require("../../../assets/fonts/NotoSansKR-Medium.otf"),
    NotoSansRegular: require("../../../assets/fonts/NotoSansKR-Regular.otf"),
  });

  if (!loaded) {
    return null;
  }

  const onChangePhone = (text) => {
    setPhone(text);
  };

  const onChangeOtp = (text) => {
    setOtp(text);
  };
  const submitCertification = () => {
    try {
      setLoadingCheckPhone(true);
      setLoadingCheckPhone(false);
      setModalVisible(true);
      setMinutes(1);
      setSeconds(0);
      setTimerOn(true);
    } catch (error) {
      Alert.alert(SERVER_ERROR);
    }
  };

  const submitConfirm = () => {
    setCredential({ user_phone: phone });
    setStep(STEP.PASSWORD);
  };
  const isDisabledPhoneButton =
    phone.trim().length !== 11 || isLoadingCheckPhone || timerOn;
  const isOutOfTime = minutes === 0 && seconds === 0;
  const isDisabledOtpButton = otp.trim().length !== 6 || isOutOfTime;

  return (
    <StyledView width={width}>
      <CustomPhoneInput onChangePhone={onChangePhone} isDisabled={timerOn} />
      <CustomButton
        title="인증요청"
        onPress={submitCertification}
        backgroundColor={isDisabledPhoneButton ? doneSubmitColor : submitColor}
        disabled={isDisabledPhoneButton}
      />
      {modalVisible && (
        <CheckPhoneMailModal
          isNewPhone={isNewPhone}
          modalVisible={modalVisible}
          closeModal={closeModal}
        />
      )}
      <Container>
        <Input
          onChange={onChangeOtp}
          keyboardType="number-pad"
          color={subGreyColor}
          borderRadius={4}
          placeholder="인증번호 입력 "
          maxLength={6}
          editable={isNewPhone}
        />
        {timerOn && (
          <Timer>
            {`0${minutes}`}:{seconds < 10 ? `0${seconds}` : seconds}
          </Timer>
        )}
      </Container>
      <CustomButton
        title="다음"
        onPress={submitConfirm}
        backgroundColor={isDisabledOtpButton ? disableColor : submitColor}
        disabled={isDisabledOtpButton}
      />
    </StyledView>
  );
};

export default SignUpWithPhone;
