import React from "react";
import StateProvider from "./StateProvider";
import SignUpStep from "./SignUpStep";
const SignUp = ({ navigation }) => {
  return (
    <StateProvider>
      <SignUpStep navigation={navigation} />
    </StateProvider>
  );
};

export default SignUp;
