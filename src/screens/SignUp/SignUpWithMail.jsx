import React, { useContext, useEffect, useState } from "react";
import { Alert, Dimensions } from "react-native";
import styled from "styled-components/native";
import { useFonts } from "expo-font";
import {
  disableColor,
  doneSubmitColor,
  subBlueColor,
  subGreyColor,
  submitColor,
} from "../../styles/colors";
import Input from "../../components/Input";
import CustomButton from "../../components/CustomButton";
import CheckPhoneMailModal from "./CheckPhoneMailModal";
import { StateContext } from "./StateProvider";
import { SERVER_ERROR, STEP } from "../../common/constants";

const Container = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 30px;
  margin-bottom: 15px;
  align-items: center;
`;
const StyledView = styled.View`
  width: 100%;
  align-self: center;
  height: 100%;
  flex: 1;
  background-color: white;
  padding-left: 20px;
  padding-right: 20px;
`;
const Timer = styled.Text`
  position: absolute;
  right: 10%;
  z-index: 99;
  color: ${subBlueColor};
`;

const SignUpWithMail = () => {
  const { setStep, setCredential } = useContext(StateContext);
  const [mail, setMail] = useState("");
  const [otp, setOtp] = useState("");
  const isNewPhone = true;
  const [modalVisible, setModalVisible] = useState(false);
  const [timerOn, setTimerOn] = useState(false);
  const [minutes, setMinutes] = useState(1);
  const [seconds, setSeconds] = useState(0);
  const [isLoadingCheckPhone, setLoadingCheckPhone] = useState(false);

  useEffect(() => {
    if (timerOn) {
      const countdown = setInterval(() => {
        if (parseInt(seconds) > 0) {
          setSeconds(parseInt(seconds) - 1);
        }
        if (parseInt(seconds) === 0) {
          if (parseInt(minutes) === 0) {
            clearInterval(countdown);
          } else {
            setMinutes(parseInt(minutes) - 1);
            setSeconds(59);
          }
        }
      }, 1000);

      return () => clearInterval(countdown);
    }
  }, [minutes, seconds, timerOn]);

  const { width, height } = Dimensions.get("window").width;
  const [loaded] = useFonts({
    NotoSansMedium: require("../../../assets/fonts/NotoSansKR-Medium.otf"),
    NotoSansRegular: require("../../../assets/fonts/NotoSansKR-Regular.otf"),
  });

  if (!loaded) {
    return null;
  }
  const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

  const isDisabled = !reg.test(mail) || mail.trim().length === 0;

  const onChangeMail = (text) => {
    setMail(text);
  };
  const onChangeOtp = (text) => {
    setOtp(text);
  };

  const submitCertification = () => {
    try {
      if (mail.trim().length === 0) {
        Alert.alert("휴대폰 번호를 입력해주세요");
        return;
      }
      setLoadingCheckPhone(true);
      setLoadingCheckPhone(false);
      setModalVisible(true);
      if (data) {
        setMinutes(1);
        setSeconds(0);
        setTimerOn(true);
      }
    } catch (error) {
      Alert.alert(SERVER_ERROR);
    }
  };

  const submitConfirm = () => {
    if (minutes === 0 && seconds === 0) {
      Alert.alert("인증을 다시 받아주세요");
      return;
    }
    setCredential({ user_mail: mail });
    setStep(STEP.PASSWORD);
  };
  const closeModal = () => setModalVisible(false);
  const isDisabledMailButton = isDisabled || isLoadingCheckPhone || timerOn;
  const isOutOfTime = minutes === 0 && seconds === 0;
  const isDisabledOtpButton = otp.trim().length !== 6 || isOutOfTime;
  return (
    <StyledView width={width} height={height}>
      <Container>
        <Input
          onChange={onChangeMail}
          keyboardType="email-address"
          color={subGreyColor}
          borderRadius={4}
          value={mail}
          placeholder="@ 이메일 주소 입력"
        />
      </Container>
      <CustomButton
        title="인증요청"
        onPress={submitCertification}
        disabled={isDisabledMailButton}
        backgroundColor={isDisabled ? doneSubmitColor : submitColor}
      />
      <CheckPhoneMailModal
        isNewPhone={isNewPhone}
        isMail
        modalVisible={modalVisible}
        closeModal={closeModal}
      />
      <Container>
        <Input
          onChange={onChangeOtp}
          keyboardType="number-pad"
          color={subGreyColor}
          borderRadius={4}
          placeholder="인증번호 입력 "
          maxLength={6}
          editable={isNewPhone}
        />
        {timerOn && (
          <Timer>
            {`0${minutes}`}:{seconds < 10 ? `0${seconds}` : seconds}
          </Timer>
        )}
      </Container>
      <CustomButton
        title="다음"
        onPress={submitConfirm}
        disabled={isDisabledOtpButton}
        backgroundColor={isDisabledOtpButton ? disableColor : submitColor}
      />
    </StyledView>
  );
};

export default SignUpWithMail;
