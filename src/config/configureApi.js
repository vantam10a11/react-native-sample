export const API = {
  LOGOUT: "/logout",
  USER: "/user",
  MESSAGE: "/message",
  CONDITION: "/condition",
  LIKE: "/like/matchuum",
  MATCHUUM_API: "/api/matchuum",
  MATCHING: "/matching",
  REPORT: "/report",
  TEACHER: "/teacher",
  STUDENT: "/student",
  FEEDBACK: "/feedback",
  REPLY: "/reply",
  BLOCK: "/block",
};
export const PUBLIC_API = [
  `${API.USER}/signin`,
  `${API.USER}/signup`,
  `${API.USER}/check-phone`,
  `${API.USER}/check-mail`,
  `${API.USER}/forget-password`,
  `${API.USER}/user-source`,
  `${API.MESSAGE}?user_phone=`,
  "/infomation/list",
  "https", //external api
];
