import 'expo-asset';
import { registerRootComponent } from "expo";
import axios from "axios";
import App from "./src/App";
import { setupAxiosInterceptors } from "./src/common/interceptors/httpInterceptor";
import { REACT_APP_AXIOS_WITH_CREDENTIALS, REACT_APP_API_BASE_URL } from "@env";

const loadConfig = async () => {
  axios.defaults.baseURL = REACT_APP_API_BASE_URL;
  axios.defaults.withCredentials = REACT_APP_AXIOS_WITH_CREDENTIALS == "true";
  axios.defaults.timeout = 10000;
  await setupAxiosInterceptors(axios);
};
loadConfig().then(() => {
  registerRootComponent(App);
});
